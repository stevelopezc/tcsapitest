# TCS API Test

## Estructura
### Base de Datos
El DBMS seleccionado para almacenar la información fue MS SQL Server. Se crearon 3 tablas:

**User**: Para almacenar nombre de usuario y clave. Para simplicar la prueba, la clave fue almacenada como un texto plano. En la vida real, la clave dentro de un sistema debe ser almacenada en base a un algoritmo de encripción que lo haga seguro. 

**Country**: Para almacenar la información de un pais, basada en [https://en.wikipedia.org/wiki/ISO_3166-1](https://en.wikipedia.org/wiki/ISO_3166-1).

**SubDivision**: Para almacenar la información de las divisiones administrativas propias de cada pais.

### API Rest
Desarrollado en C# y Net Core 3.1 sobre la plantilla de Web API. Se crea un controlador para el maestro y acciones para las operaciones básicas tanto para el maestro (**Country**) como para el detalle (**SubDivisions**). Se utiliza el formato JSON para devolver la información a los diferentes clientes.

Como parte de la solución, se habilito CORS para permitir llamadas desde localhost, aca deberia ser remplazado por el listado de dominios y subdominios desde los cuales es permitido realizar las llamadas al API, a travez de una politica.

Adicionalmente, para realizar modificaciones al modelo de datos, se utiliza Entity Framework Net Core lo que permite mapear las entidades definidas dentro de clases en tablas de la base datos. Adicionalmente, se genera modulo para aplicar migraciones y/o popular las tablas con valores iniciales.

Se documenta el uso del API utilizando el estandard OpenAPI, para ello se implementa Swagger a travez del NuGet Swashbuckle que permite el uso de OpenApi y todos sus propiedades.

Para consumir el API, se requiere tener un token de autenticación, el cual se genera al contar con un usuario y clave valida. Para efectos de esta prueba, se pueden utilizar las siguientes credenciales:

**Usuario:** *loadTestUser*
**Clave:** *@dminT3st*

Se incluyen pruebas unitarias y de integración. Las pruebas unitarias se encuentran en el proyecto *XUnitTestTCS* y las de integración en el proyecto *XUnitTestTCSIntegration*. Para efectos de esta evaluación, se simplifica la complejidad de las pruebas dejandolas con validaciones muy básicas. En un escenario real, las pruebas deben de corresponder a la complejidad de la funcionalidad que se este validando.

## Como utilizar el Proyecto
El fuente del proyecto se encuentra versionado, en un repositorio dentro de GitLab. Se hace necesario clonarlo en un repositorio local para irle incorporando mejoras y/o cambios. Se recomienda hacerlo a traves de HTTPS, usando la siguiente [URL](https://gitlab.com/stevelopezc/tcsapitest.git).

Para clonar el proyecto, tenemos la opcion de hacerlo a travez de comandos de Git. Si no  es familiar el uso de esta herramienta, es posible consultar la documentacion oficial [aqui](https://git-scm.com/docs/git). La otra forma que se tiene es a travez del propio Visual Studio. Para ello ir al Menu **Archivo (File)**, y seleccionar la opcion **Clonar Repositorio (Clone Repository)**, al abrir la ventana deberemos ingresar la direccion URL del repositorio asi como la carpeta donde se quiere descargar el repositorio remoto.

Luego de clonar el proyecto, se debe abrir el archivo de la solucion **testApi.sln** que se encuentra en el directorio principal del proyecto recien clonado, haciendo doble click sobre el. O bien, abrir Visual Studio 2019 e ir a *Archivo*->*Open*->*Solucion/proyecto*, en el cuadro de dialogo navegar hasta donde se encuentre el archivo de la solucion.

Como ya se menciono, el proyecto usa una base de datos de SQL Server. La DB no es necesaria que se cree, ya que el proyecto cuenta con migraciones automaticas que se encargan de crear las tablas necesarias para que funcione la aplicacion. Para que esto funcione, es necesario modificar el archivo de configuracion **appsettings.json**, ubicado en la raiz del proyecto **testApi**. Al abrirlo, debemos de modificar la informacion de conexion con la base de datos que se encuentra en la seccion *ConnectionString*, bajo el nombre de **DefaultConnection** con los siguientes datos:

Server=[NOMBRE SERVIDOR];Initial Catalog=[NOMBRE DE LA BASE DE DATOS];User ID=[USUARIO];Password=[CLAVE]

**NOMBRE SERVIDOR**: Reemplazar con el nombre del servidor o de la maquina donde estara alojada la base de datos.<br />
**NOMBRE DE LA BASE DE DATOS**: Reemplazar con el nombre de la DB a utilizar.<br />
**USUARIO**: Usuario de SQL con el cual se conectara al servidor. <br />
**CLAVE**: Clave del usuario para conexion. <br />

Para ejecutar el proyecto, basta con presionar *F5* o ir al menu *Debug*->*Start Debugging*. Si se esta ejecutando el proyecto desde Windows, se puede escoger entre ejecutarlo sobre IIS Express o como aplicación. Lo primero que se vera al estar corriendo el proyecto, sera el swagger con la informacion de cada una de las apis disponibles las cuales pueden ser probadas directamente. Las API's, estan protegidas con JWT, con lo que se necesita obtener un token para poder usarlo. Este se obtiene en el API **Login**, ingresando un usuario/clave valida. Una vez generado el Token, este debe de ingresarse en el boton **Authorize** siguiendo las indicaciones.

Para documentar el API, como ya se menciono se usa Swagger. El proyecto tiene configurado que genere un archivo de documentacion en formato XML. Este es utilizado para exponer la informacion de todos y cada uno de los controladores y acciones que incluye el proyecto. Se utiliza el siguiente formato:

/// < summary ><br />
/// *Aca va la descripcion de la accion*<br />
/// </ summary ><br />
/// < returns >*Describe lo que la accion retorna como resultado*</ returns><br />
/// < response code="*Codigo de Respuesta*">*Descripcion de la respuesta*</ response><br />
                         .<br />
                         .<br />
                         .<br />
/// < response code="*Codigo de Respuesta XXX*">*Descripcion*</ response><br />

Para mas detalle acerca de Swagger y como usarlo, se puede consultar la documentacion oficial [aqui](https://swagger.io/specification/)

El proyecto esta escrito enteramente en C#, para mayor informacion se sugiere consultar la documentacion proveida por Microsoft [aca](https://docs.microsoft.com/en-us/dotnet/csharp/). Para realizar las consultas a la base de datos se usa LINQ, en esta [URL](https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/concepts/linq/) se podra encontrar mayor informacion.

Se sugiere ademas, una lectura rapida de *Contenedores* (**Docker**) si el desarrollo se hiciese en un SO iOS para montar el servidor de SQL Server (documentacion oficial [aca](https://docs.docker.com/)). Si se requiere una guia para instalar la instancia de SQL Server en iOS, se puede usar [esta](https://database.guide/how-to-install-sql-server-on-a-mac/).

Adicionalmente, nunca esta demas reforzar los conocimientos en Rutas dentro de las acciones en los controladores leyendo el recurso proveido por Microsoft [aca](https://docs.microsoft.com/en-us/aspnet/core/mvc/controllers/routing?view=aspnetcore-3.1).

**Happy coding!!!!**






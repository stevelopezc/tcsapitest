﻿namespace XUnitTestTCS
{
    using System;
    using System.Linq;
    using testApi.Data;
    using testApi.Entities;

    public static class DBContextExtension
    {
        public static void DBSeed(this testApiContext _context)
        {

            _context.Countries.Add(new Country
            {
                Alpha2 = "GT",
                Alpha3 = "GTM",
                Code = 340,
                ISO_3166_2 = "https://en.wikipedia.org/wiki/ISO_3166-2:GT",
                Is_Independent = true,
                Name = "Guatemala",
                Created_At = DateTimeOffset.Now,
                Updated_At = DateTimeOffset.Now
            });

            _context.Countries.Add(new Country
            {
                Alpha2 = "SV",
                Alpha3 = "SLV",
                Code = 222,
                ISO_3166_2 = "https://en.wikipedia.org/wiki/ISO_3166-2:SV",
                Is_Independent = true,
                Name = "El Salvador",
                Created_At = DateTimeOffset.Now,
                Updated_At = DateTimeOffset.Now
            });

            _context.SaveChanges();

            var item = _context.Countries.FirstOrDefault(d => d.Name == "Guatemala");
            _context.SubDivisions.Add(new SubDivisionItemReturn
            {
                Code = "GTM",
                CountryId = item.Id,
                Name = "Guatemala",
                Created_At = DateTimeOffset.Now,
                Updated_At = DateTimeOffset.Now
            });

            _context.SubDivisions.Add(new SubDivisionItemReturn
            {
                Code = "SAQ",
                CountryId = item.Id,
                Name = "Sacatepequez",
                Created_At = DateTimeOffset.Now,
                Updated_At = DateTimeOffset.Now
            });


            _context.Users.Add(new User
            {
                UserName = "loadTestUser",
                Password = "@dminT3st",
                Created_At = DateTimeOffset.Now,
                Updated_At = DateTimeOffset.Now
            });

            _context.SaveChanges();

        }

    }
}

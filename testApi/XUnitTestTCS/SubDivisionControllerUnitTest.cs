﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using testApi.Controllers;
using testApi.Entities;
using testApi.Models;
using Xunit;

namespace XUnitTestTCS
{
    public class SubDivisionControllerUnitTest
    {
        [Fact]
        public async Task TestGetSubDivisions()
        {
            var _context = DBMocker.GetTestApiContext(nameof(TestGetSubDivisions));
            var controller = new SubDivisionsController(_context);

            var response = await controller.GetSubDivisions();
            var result = response.Result as OkObjectResult;
            _context.Dispose();

            Assert.NotNull(result.Value as IEnumerable<SubDivision>);
        }

        [Fact]
        public async Task TestGetSubDivisionsByCountry()
        {
            var _context = DBMocker.GetTestApiContext(nameof(TestGetSubDivisions));
            var controller = new SubDivisionsController(_context);

            var response = await controller.GetSubDivisionsByCountry(1);
            var result = response.Result as OkObjectResult;
            _context.Dispose();

            Assert.True((result.Value as SearchSubdivisionResultModel).ItemsFound > 0);
        }

        [Fact]
        public async Task TestGetSubDivision()
        {
            var _context = DBMocker.GetTestApiContext(nameof(TestGetSubDivisions));
            var controller = new SubDivisionsController(_context);

            var response = await controller.GetSubDivision(1);
            var result = response.Result as OkObjectResult;
            _context.Dispose();

            Assert.NotNull(result);
        }

        [Fact]
        public async Task TestPostSubDivision()
        {
            var _context = DBMocker.GetTestApiContext(nameof(TestGetSubDivisions));
            var controller = new SubDivisionsController(_context);

            var subDivision = new SubDivisionNewItem
            {
                Code = "SM",
                Name = "San Marco"
            };

            var response = await controller.PostSubDivision(subDivision);
            var result = response.Result as CreatedAtActionResult;
            _context.Dispose();

            Assert.NotNull(result.Value);
        }

        [Fact]
        public async Task TestPutSubDivision()
        {
            var _context = DBMocker.GetTestApiContext(nameof(TestGetSubDivisions));
            var controller = new SubDivisionsController(_context);

            var subDivision = new SubDivisionNewItem
            {
                Id=3,
                Code = "SM",
                Name = "San Marcos"
            };

            var response = await controller.PutSubDivision(3, subDivision);
            var result = response as OkObjectResult;
            _context.Dispose();

            Assert.NotNull(result);
        }

        [Fact]
        public async Task TestDeleteSubDivision()
        {
            var _context = DBMocker.GetTestApiContext(nameof(TestGetSubDivisions));
            var controller = new SubDivisionsController(_context);            

            var response = await controller.DeleteSubDivision(2);
            var result = response.Result as OkObjectResult;
            _context.Dispose();

            Assert.NotNull(result);
        }

        
    }
}

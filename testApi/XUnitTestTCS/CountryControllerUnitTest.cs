﻿namespace XUnitTestTCS
{
    using Microsoft.AspNetCore.Mvc;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using testApi.Controllers;
    using testApi.Entities;
    using testApi.Models;
    using Xunit;

    public class CountryControllerUnitTest
    {
        #region Country
        [Fact]
        public async Task TestGetCountries()
        {
            var _context = DBMocker.GetTestApiContext(nameof(TestGetCountries));
            var controller = new CountriesController(_context);

            var response = await controller.GetCountries();
            var result = response.Result as OkObjectResult;
            _context.Dispose();

            Assert.NotNull(result.Value as IEnumerable<Country>);
        }

        [Fact]
        public async Task TestGetCountriesByFilter()
        {
            var _context = DBMocker.GetTestApiContext(nameof(TestGetCountries));
            var controller = new CountriesController(_context);

            var response = await controller.GetCountriesByFilter("GU", string.Empty);
            var result = response.Result as OkObjectResult;
            _context.Dispose();

            Assert.True((result.Value as SearchCountryResultModel).ItemsFound > 0);
        }

        [Fact]
        public async Task TestGetCountry()
        {
            var _context = DBMocker.GetTestApiContext(nameof(TestGetCountries));
            var controller = new CountriesController(_context);

            var response = await controller.GetCountry(1);
            var result = response.Result as OkObjectResult;
            _context.Dispose();

            Assert.NotNull(result);
        }

        [Fact]
        public async Task TestPostCountry()
        {
            var _context = DBMocker.GetTestApiContext(nameof(TestGetCountries));
            var controller = new CountriesController(_context);

            var country = new CountryNewItem
            {
                Alpha2 = "HN",
                Alpha3 = "HND",
                Code = 340,
                Is_Independent = true,
                ISO_3166_2 = "https://en.wikipedia.org/wiki/ISO_3166-2:HN",
                Name = "Honduras"
            };

            var response = await controller.PostCountry(country);
            var result = response.Result as CreatedAtActionResult;
            _context.Dispose();

            Assert.NotNull(result.Value);
        }

        [Fact]
        public async Task TestPutCountry()
        {
            var _context = DBMocker.GetTestApiContext(nameof(TestGetCountries));
            var controller = new CountriesController(_context);

            var country = new CountryNewItem
            {

                Alpha2 = "GT",
                Alpha3 = "GTM",
                Code = 320,
                Is_Independent = true,
                ISO_3166_2 = "https://en.wikipedia.org/wiki/ISO_3166-2:GT",
                Name = "Guatemala CA"
            };

            var response = await controller.PutCountry(1, country);
            var result = response as OkObjectResult;
            _context.Dispose();

            Assert.NotNull(result);
        }

        [Fact]
        public async Task TestDeleteCountry()
        {
            var _context = DBMocker.GetTestApiContext(nameof(TestGetCountries));
            var controller = new CountriesController(_context);

            var response = await controller.DeleteCountry(2);
            var result = response.Result as OkObjectResult;
            _context.Dispose();

            Assert.NotNull(result);
        }

        [Fact]
        public async Task TestDeleteWithSubDivisionsCountry()
        {
            var _context = DBMocker.GetTestApiContext(nameof(TestGetCountries));
            var controller = new CountriesController(_context);

            var response = await controller.DeleteCountry(1);
            var result = response.Result as BadRequestObjectResult;
            _context.Dispose();

            Assert.NotNull(result);
        }
        #endregion

        #region SubDivisions

        [Fact]
        public async Task TestGetSubDivisionsByCountry()
        {
            var _context = DBMocker.GetTestApiContext(nameof(TestGetSubDivisionsByCountry));
            var controller = new CountriesController(_context);

            var response = await controller.GetSubDivisionsByCountry(1);
            var result = response.Result as OkObjectResult;
            _context.Dispose();

            Assert.True((result.Value as SearchSubdivisionResultModel).ItemsFound > 0);
        }

        [Fact]
        public async Task TestGetSubDivision()
        {
            var _context = DBMocker.GetTestApiContext(nameof(TestGetSubDivision));
            var controller = new CountriesController(_context);

            var response = await controller.GetSubDivision(1);
            var result = response.Result as OkObjectResult;
            _context.Dispose();

            Assert.NotNull(result);
        }

        [Fact]
        public async Task TestPutSubDivision()
        {
            var _context = DBMocker.GetTestApiContext(nameof(TestPutCountry));
            var controller = new CountriesController(_context);

            var subDivision = new SubDivisionNewItem
            {

                Code = "SM",
                Name = "San Marcos"                
            };

            var response = await controller.PutSubDivision(3, subDivision);
            var result = response as OkObjectResult;
            _context.Dispose();

            Assert.NotNull(result);
        }

        [Fact]
        public async Task TestDeleteSubDivision()
        {
            var _context = DBMocker.GetTestApiContext(nameof(TestDeleteSubDivision));
            var controller = new CountriesController(_context);

            var response = await controller.DeleteSubDivision(2);
            var result = response.Result as OkObjectResult;
            _context.Dispose();

            Assert.NotNull(result);
        }
        #endregion
    }
}

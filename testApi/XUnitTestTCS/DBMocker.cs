﻿namespace XUnitTestTCS
{
    using Microsoft.EntityFrameworkCore;
    using testApi.Data;

    public static class DBMocker
    {
        public static testApiContext GetTestApiContext(string dbName)
        {
            // Create options for DbContext instance
            var options = new DbContextOptionsBuilder<testApiContext>()
                .UseInMemoryDatabase(databaseName: dbName)
                .Options;

            // Create instance of DbContext
            var dbContext = new testApiContext(options);

            // Add entities in memory
            dbContext.DBSeed();

            return dbContext;
        }

    }
}

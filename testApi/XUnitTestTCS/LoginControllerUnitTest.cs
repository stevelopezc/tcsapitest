﻿namespace XUnitTestTCS
{
    using Microsoft.AspNetCore.Mvc;
    using System.Threading.Tasks;
    using testApi.Controllers;
    using testApi.Models;
    using Xunit;

    public class LoginControllerUnitTest
    {
        [Fact]
        public async Task TestAuthenticateFail()
        {
            var _context = DBMocker.GetTestApiContext(nameof(TestAuthenticateFail));
            var controller = new LoginController(_context);

            var response = await controller.Authenticate(new LoginModel { Password = "pass", UserName= "loadTestUser" });
            var result = response.Result as UnauthorizedObjectResult;
            _context.Dispose();

            Assert.NotNull(result);
        }

        [Fact]
        public async Task TestAuthenticate()
        {
            var _context = DBMocker.GetTestApiContext(nameof(TestAuthenticateFail));
            var controller = new LoginController(_context);

            var response = await controller.Authenticate(new LoginModel { Password = "@dminT3st", UserName = "loadTestUser" });
            var result = response.Result as OkObjectResult;
            _context.Dispose();

            Assert.NotNull(result);
        }
    }
}

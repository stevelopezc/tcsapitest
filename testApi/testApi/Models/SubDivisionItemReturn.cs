﻿namespace testApi.Models
{
    using Newtonsoft.Json;

    public class SubDivisionNewItemReturn
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("code")]
        public string Code { get; set; }
        
    }
}

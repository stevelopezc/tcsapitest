﻿namespace testApi.Models
{
    using Newtonsoft.Json;

    public class SubDivisionNewItem
    {

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("code")]
        public string Code { get; set; }
        
    }
}

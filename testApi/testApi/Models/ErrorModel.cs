﻿namespace testApi.Models
{
    using Newtonsoft.Json;

    public class ErrorModel
    {
        [JsonProperty("isError")]
        public bool IsError { get; set; }

        [JsonProperty("errorMessage")]
        public string ErrorMessage { get; set; }
    }
}

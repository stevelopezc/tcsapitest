﻿
namespace testApi.Models
{
    using Newtonsoft.Json;
    using System;

    public class TokenAuthorizationModel
    {
        [JsonProperty("token")]
        public string Token { get; set; }

        [JsonProperty("expiration")]
        public DateTimeOffset Expiration { get; set; }
    }
}

﻿
namespace testApi.Models
{
    using System.Collections.Generic;
    using testApi.Entities;

    public class SearchCountryResultModel
    {
        public SearchCountryResultModel()
        {
            CountryList = new List<CountryItemReturn>();
        }
        public int ItemsFound { get; set; }
        public List<CountryItemReturn> CountryList { get; set; }
    }

    public class SearchSubdivisionResultModel
    {
        public SearchSubdivisionResultModel()
        {
            SubDivisionsList = new List<SubDivisionNewItemReturn>();
        }
        public int ItemsFound { get; set; }
        public List<SubDivisionNewItemReturn> SubDivisionsList { get; set; }
    }
}

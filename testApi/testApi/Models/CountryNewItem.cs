﻿namespace testApi.Models
{
    using Newtonsoft.Json;
    using System.ComponentModel.DataAnnotations;

    public class CountryNewItem
    {       
        [JsonProperty("name")]
        [Required]
        public string Name { get; set; }

        [JsonProperty("alpha2")]
        [Required]
        public string Alpha2 { get; set; }

        [JsonProperty("alpha3")]
        public string Alpha3 { get; set; }

        [JsonProperty("code")]
        public int Code { get; set; }

        [JsonProperty("iso_3166_2")]
        public string ISO_3166_2 { get; set; }

        [JsonProperty("is_independent")]
        [Required]
        public bool Is_Independent { get; set; }
    }
}

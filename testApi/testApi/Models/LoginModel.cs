﻿namespace testApi.Models
{
    using Newtonsoft.Json;
    using System.ComponentModel.DataAnnotations;

    public class LoginModel
    {
        [JsonProperty("userName")]
        [Required]
        public string UserName { get; set; }
        
        [JsonProperty("password")]
        [Required]
        public string Password { get; set; }
    }
}

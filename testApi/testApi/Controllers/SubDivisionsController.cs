﻿

namespace testApi.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Authentication.JwtBearer;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Cors;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.EntityFrameworkCore;
    using Swashbuckle.AspNetCore.Annotations;
    using testApi.Data;
    using testApi.Entities;
    using testApi.Models;

    /// <summary>
    /// Controller for Actions on SubDivision
    /// </summary>
    
    [EnableCors("TCSTestApiOrigins")]
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class SubDivisionsController : ControllerBase
    {
        private readonly testApiContext _context;

        public SubDivisionsController(testApiContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Get all subdivisions
        /// </summary>
        /// <returns>A subdivision List</returns>
        /// <response code="200">Returns subdivisions List</response>
        /// <response code="400">Get some Database error.</response>
        /// <response code="401">User doesn't have authorization to access.</response>
        // GET: api/SubDivisions
        [HttpGet]
        
        public async Task<ActionResult<IEnumerable<SubDivision>>> GetSubDivisions()
        {
            try
            {
                return Ok(await _context.SubDivisions.Include(d => d.Country).ToListAsync());
            }
            catch (Exception ex)
            {
                return BadRequest(new ErrorModel { IsError = true, ErrorMessage = ex.Message + " " + ex.InnerException?.Message });
            }
        }

        /// <summary>
        /// Get all subdivisions by Country
        /// </summary>
        /// <param name="countryId">Country id to search</param>
        /// <returns>An entity with the results count and subdivisions List for a country id</returns>
        /// <response code="200">Returns entity with the results count and subdivisions List for a country id</response>
        /// <response code="400">Get some Database error.</response>
        /// <response code="401">User doesn't have authorization to access.</response>
        // GET: api/SubDivisions/GetSubDivisionsByCountry
        [HttpGet]
        [Route("countries/{countryId}/SubDivisions")]
        public async Task<ActionResult<SearchCountryResultModel>> GetSubDivisionsByCountry(int countryId)
        {
            try
            {
                List<SubDivision> subDivisions = await _context.SubDivisions.Include(d => d.Country).Where(d => d.CountryId == countryId).ToListAsync();
                return Ok(new SearchSubdivisionResultModel { SubDivisionsList = subDivisions, ItemsFound = subDivisions.Count });
            }
            catch (Exception ex)
            {
                return BadRequest(new ErrorModel { IsError = true, ErrorMessage = ex.Message + " " + ex.InnerException?.Message });
            }
        }

        /// <summary>
        /// Get a SubDivision by Id
        /// </summary>
        /// <param name="id">SubDivision Id</param>
        /// <returns>SubDivision entity</returns>
        /// <response code="200">Returns the SubDivision entity</response>
        /// <response code="400">Get some Database error.</response>
        /// <response code="401">User doesn't have authorization to access.</response>
        /// <response code="404">If the SubDivision Id don't exist. An error entity is sent.</response>
        // GET: api/SubDivisions/5
        [HttpGet("{id}")]
        public async Task<ActionResult<SubDivision>> GetSubDivision(int id)
        {
            try
            {
                var subDivision = await _context.SubDivisions.Include(d => d.Country).FirstOrDefaultAsync(d => d.Id == id);

                if (subDivision == null)
                {
                    return NotFound(new ErrorModel { IsError = true, ErrorMessage = "SubDivision doesn't exist." });
                }

                return Ok(subDivision);
            }
            catch (Exception ex)
            {

                return BadRequest(new ErrorModel { IsError = true, ErrorMessage = ex.Message + " " + ex.InnerException?.Message });
            }
        }

        /// <summary>
        /// Update SubDivision information
        /// </summary>
        /// <param name="id">SubDivision Id</param>
        /// <param name="subDivision">SubDivision entity to be Updated</param>
        /// <returns>SubDivision entity modified</returns>
        /// <response code="200">Returns the SubDivision entity updated</response>
        /// <response code="400">An error entity is sent.<br />If trying to update the name and is already assigned to other SubDivision entity.<br />If trying to update a SubDivision entity and doesn't exist.<br />If any error showed up during DB update operation.</response>
        /// <response code="401">User doesn't have authorization to access.</response>
        // PUT: api/SubDivisions/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSubDivision(int id, SubDivisionNewItem subDivision)
        {
            SubDivision entity = _context.SubDivisions.FirstOrDefault(d => d.Id == id);
            if (entity == null)
                return BadRequest(new ErrorModel { IsError = true, ErrorMessage = "SubDivision Id doesn't exist." });

            entity.Name = subDivision.Name;
            entity.Code = subDivision.Code;
            entity.Updated_At = DateTimeOffset.Now;

            try
            {
                await _context.SaveChangesAsync();
                return Ok(entity);
            }
            catch (Exception ex)
            {
                return BadRequest(new ErrorModel { IsError = true, ErrorMessage = ex.Message + " " + ex.InnerException?.Message });
            }
        }

        /// <summary>
        /// Create new SubDivision entity
        /// </summary>
        /// <param name="subDivision">SubDivision entity to create</param>
        /// <returns>New SubDivision entity</returns>
        /// <response code="201">Return new SubDivision entity.</response>
        /// <response code="400">If any error showed up during DB update operation</response>
        /// <response code="401">User doesn't have authorization to access.</response>
        /// <response code="404">SubDivision Id doesn't exist.</response>
        // POST: api/SubDivisions
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<SubDivision>> PostSubDivision(SubDivisionNewItem subDivision)
        {
            try
            {
                SubDivision division = new SubDivision
                {
                    Code = subDivision.Code,
                    CountryId = subDivision.CountryId,
                    Name = subDivision.Name,
                    Created_At = DateTimeOffset.Now,
                    Updated_At = DateTimeOffset.Now
                };


                _context.SubDivisions.Add(division);
                await _context.SaveChangesAsync();

                return CreatedAtAction("GetSubDivision", new { id = subDivision.Id }, subDivision);
            }
            catch (Exception ex)
            {
                return BadRequest(new ErrorModel { IsError = true, ErrorMessage = ex.Message + " " + ex.InnerException?.Message });
            }
        }

        /// <summary>
        /// Delete a SubDivision entity
        /// </summary>
        /// <param name="id">SubDivision id to delete</param>
        /// <returns>New SubDivision entity</returns>
        /// <response code="201">Return the deleted SubDivision entity.</response>
        /// <response code="400">If any error showed up during DB update operation</response>
        /// <response code="401">User doesn't have authorization to access.</response>
        /// <response code="404">SubDivision Id doesn't exist.</response>
        // DELETE: api/SubDivisions/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<SubDivision>> DeleteSubDivision(int id)
        {
            try
            {
                var subDivision = await _context.SubDivisions.FindAsync(id);
                if (subDivision == null)
                {
                    return NotFound(new ErrorModel { IsError = true, ErrorMessage = "SubDivision Id doesn't exist." });
                }

                _context.SubDivisions.Remove(subDivision);
                await _context.SaveChangesAsync();

                return Ok(subDivision);
            }
            catch (Exception ex)
            {

                return BadRequest(new ErrorModel { IsError = true, ErrorMessage = ex.Message + " " + ex.InnerException?.Message });
            }
        }

        private bool SubDivisionExists(int id)
        {
            return _context.SubDivisions.Any(e => e.Id == id);
        }        
    }
}

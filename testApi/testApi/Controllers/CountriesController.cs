﻿namespace testApi.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Authentication.JwtBearer;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Cors;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.EntityFrameworkCore;
    using Swashbuckle.AspNetCore.Annotations;
    using testApi.Data;
    using testApi.Entities;
    using testApi.Models;

    /// <summary>
    /// Controller for Actions on Country
    /// </summary>
    [EnableCors("TCSTestApiOrigins")]
    [Route("api/[controller]")]   
    [Produces("application/json")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]    
    public class CountriesController : ControllerBase
    {
        private readonly testApiContext _context;
        const string tagSubDivision = "Country SubDivisions";

        public CountriesController(testApiContext context)
        {
            _context = context;
        }
        #region Country Region

        /// <summary>
        /// Get all countries
        /// </summary>
        /// <returns>A country List</returns>
        /// <response code="200">Returns a country list</response>
        /// <response code="400">Get some Database error.</response>
        /// <response code="401">User doesn't have authorization to access.</response>
        // GET: api/Countries
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CountryItemReturn>>> GetCountries()
        {
            try
            {
                List<CountryItemReturn> countryItems = new List<CountryItemReturn>();
                var countries = await _context.Countries.ToListAsync();
                countries.ForEach(d =>
                {
                    countryItems.Add(new CountryItemReturn
                    {
                        Alpha2 = d.Alpha2,
                        Alpha3 = d.Alpha3,
                        Code = d.Code,
                        Id = d.Id,
                        ISO_3166_2 = d.ISO_3166_2,
                        Is_Independent = d.Is_Independent,
                        Name = d.Name
                    });
                });


                return Ok(countryItems);
            }
            catch (Exception ex)
            {
                return BadRequest(new ErrorModel { IsError = true, ErrorMessage = ex.Message + " " + ex.InnerException?.Message });
            }
        }



        /// <summary>
        /// Get a Country by Id
        /// </summary>
        /// <param name="id">Country Id</param>
        /// <returns>Country item</returns>
        /// <response code="200">Returns the Country entity</response>
        /// <response code="400">Get some Database error.</response>
        /// <response code="401">User doesn't have authorization to access.</response>
        /// <response code="404">If the Country Id don't exist. An error entity is sent.</response>
        // GET: api/Countries/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CountryItemReturn>> GetCountry(int id)
        {
            try
            {
                var country = await _context.Countries.Include(d => d.SubDivisions).FirstOrDefaultAsync(d => d.Id == id);

                if (country == null)
                {
                    return NotFound(new ErrorModel { IsError = true, ErrorMessage = "Country doesn't exist." });
                }

                CountryItemReturn countryItem = new CountryItemReturn
                {
                    Id = country.Id,
                    Alpha2 = country.Alpha2,
                    Alpha3 = country.Alpha3,
                    Code = country.Code,
                    ISO_3166_2 = country.ISO_3166_2,
                    Is_Independent = country.Is_Independent,
                    Name = country.Name
                };
                return Ok(countryItem);
            }
            catch (Exception ex)
            {

                return BadRequest(new ErrorModel { IsError = true, ErrorMessage = ex.Message + " " + ex.InnerException?.Message });
            }
        }

        /// <summary>
        /// Get all countries by Filter
        /// </summary>
        /// <param name="name">Country name to search on Countries (CONTAINS)</param>
        /// <param name="alpha2">Country Alpha to search</param>
        /// <returns>An entity with the results count and countries List</returns>
        /// <response code="200">Returns an entity contains the results count and countries List</response>
        /// <response code="400">Get some Database error.</response>
        /// <response code="401">User doesn't have authorization to access.</response>
        // GET: api/Countries/GetCountriesByFilter
        [HttpGet("name={name}&alpha2={alpha2}")]
        public async Task<ActionResult<SearchCountryResultModel>> GetCountriesByFilter(string name, string alpha2)
        {
            try
            {
                List<Country> countries = await _context.Countries.Where(d => (string.IsNullOrEmpty(name) || d.Name.ToUpper().Contains(name.ToUpper())) && (string.IsNullOrEmpty(alpha2) || d.Alpha2.ToUpper() == alpha2.ToUpper())).ToListAsync();
                List<CountryItemReturn> countryItems = new List<CountryItemReturn>();
                countries.ForEach(d =>
                {
                    countryItems.Add(new CountryItemReturn
                    {
                        Alpha2 = d.Alpha2,
                        Alpha3 = d.Alpha3,
                        Code = d.Code,
                        Id = d.Id,
                        ISO_3166_2 = d.ISO_3166_2,
                        Is_Independent = d.Is_Independent,
                        Name = d.Name
                    });
                });

                return Ok(new SearchCountryResultModel { CountryList = countryItems, ItemsFound = countries.Count });
            }
            catch (Exception ex)
            {
                return BadRequest(new ErrorModel { IsError = true, ErrorMessage = ex.Message + " " + ex.InnerException?.Message });
            }
        }

        /// <summary>
        /// Update Country information
        /// </summary>
        /// <param name="id">Country Id</param>
        /// <param name="countryItem">Country entity to be Updated</param>
        /// <returns>Country entity modified</returns>
        /// <response code="200">Returns the Country entity updated</response>
        /// <response code="400">An error entity is sent.<br />If trying to update the name or alpha2 code and some of them is already assigned to other Country entity.<br />If trying to update a Country entity and doesn't exist.<br />If any error showed up during DB update operation.</response>
        /// <response code="401">User doesn't have authorization to access.</response>
        // PUT: api/Countries/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCountry(int id, CountryNewItem countryItem)
        {

            Country entity = _context.Countries.FirstOrDefault(d => d.Id == id);
            if (entity == null)
                return BadRequest(new ErrorModel { IsError = true, ErrorMessage = "Country Id doesn't exist." });

            entity.Name = countryItem.Name;
            entity.Alpha2 = countryItem.Alpha2;
            entity.Alpha3 = countryItem.Alpha3;
            entity.Code = countryItem.Code;
            entity.ISO_3166_2 = countryItem.ISO_3166_2;
            entity.Is_Independent = countryItem.Is_Independent;
            entity.Updated_At = DateTimeOffset.Now;

            try
            {
                await _context.SaveChangesAsync();
                return Ok(countryItem);
            }
            catch (Exception ex)
            {
                return BadRequest(new ErrorModel { IsError = true, ErrorMessage = ex.Message + " " + ex.InnerException?.Message });
            }
        }

        /// <summary>
        /// Create new Country entity
        /// </summary>
        /// <param name="countryItem">Country entity to create</param>
        /// <returns>New contry entity</returns>
        /// <response code="201">Return new Country entity.</response>
        /// <response code="400">If any error showed up during DB update operation</response>
        /// <response code="401">User doesn't have authorization to access.</response>
        /// <response code="404">Country Id doesn't exist.</response>
        // POST: api/Countries
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<CountryItemReturn>> PostCountry(CountryNewItem countryItem)
        {
            try
            {
                if (CountryExists(countryItem.Name, countryItem.Alpha2))
                    return BadRequest(new ErrorModel { IsError = true, ErrorMessage = "The name or alpha2 code is already assigned to other Country entity" });

                Country country = new Country
                {
                    Alpha2 = countryItem.Alpha2,
                    Alpha3 = countryItem.Alpha3,
                    Code = countryItem.Code,
                    ISO_3166_2 = countryItem.ISO_3166_2,
                    Is_Independent = countryItem.Is_Independent,
                    Name = countryItem.Name,
                    Created_At = DateTimeOffset.Now,
                    Updated_At = DateTimeOffset.Now
                };   
                country.Created_At = DateTimeOffset.Now;
                country.Updated_At = DateTimeOffset.Now;

                _context.Countries.Add(country);
                await _context.SaveChangesAsync();

                CountryItemReturn countryNewItem = new CountryItemReturn
                {
                    Id = country.Id,
                    Alpha2 = country.Alpha2,
                    Alpha3 = country.Alpha3,
                    Code = country.Code,
                    ISO_3166_2 = country.ISO_3166_2,
                    Is_Independent = country.Is_Independent,
                    Name = country.Name
                };

                return CreatedAtAction("GetCountry", new { id = country.Id }, countryNewItem);
            }
            catch (Exception ex)
            {
                return BadRequest(new ErrorModel { IsError = true, ErrorMessage = ex.Message + " " + ex.InnerException?.Message });
            }
        }

        /// <summary>
        /// Delete a Country entity
        /// </summary>
        /// <param name="id">Country id to delete</param>
        /// <returns>New contry entity</returns>
        /// <response code="201">Return the deleted Country entity.</response>
        /// <response code="400">If any error showed up during DB update operation</response>
        /// <response code="401">User doesn't have authorization to access.</response>
        /// <response code="404">Country Id doesn't exist.</response>
        // DELETE: api/Countries/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<CountryNewItem>> DeleteCountry(int id)
        {
            try
            {
                var country = await _context.Countries.Include(d => d.SubDivisions).FirstOrDefaultAsync(d => d.Id == id);
                if (country == null)
                {
                    return NotFound(new ErrorModel { IsError = true, ErrorMessage = "Country Id doesn't exist." });
                }

                if (country.SubDivisions.Count > 0)
                {
                    return BadRequest(new ErrorModel { IsError = true, ErrorMessage = "Country has SubDivisions. It can't be deleted." });
                }


                CountryNewItem item = new CountryNewItem
                {
                    Alpha2 = country.Alpha2,
                    Alpha3 = country.Alpha3,
                    Code = country.Code,
                    ISO_3166_2 = country.ISO_3166_2,
                    Is_Independent = country.Is_Independent,
                    Name = country.Name
                };

                _context.Countries.Remove(country);
                await _context.SaveChangesAsync();

                

                return Ok(item);
            }
            catch (Exception ex)
            {

                return BadRequest(new ErrorModel { IsError = true, ErrorMessage = ex.Message + " " + ex.InnerException?.Message });
            }
        }
        #endregion

        /// <summary>
        /// Get all subdivisions by Country
        /// </summary>
        /// <param name="countryId">Country id to search</param>
        /// <returns>An entity with the results count and subdivisions List for a country id</returns>
        /// <response code="200">Returns entity with the results count and subdivisions List for a country id</response>
        /// <response code="400">Get some Database error.</response>
        /// <response code="401">User doesn't have authorization to access.</response>
        // GET: api/SubDivisions/GetSubDivisionsByCountry
        [HttpGet]
        [SwaggerOperation(Tags = new[] { tagSubDivision})]
        [Route("{countryId}/SubDivisions")]
        public async Task<ActionResult<SearchSubdivisionResultModel>> GetSubDivisionsByCountry(int countryId)
        {
            try
            {
                var subDivisions = await _context.SubDivisions.Where(d => d.CountryId == countryId).ToListAsync();
                List<Models.SubDivisionNewItemReturn> subDivisionItems = new List<Models.SubDivisionNewItemReturn>();
                subDivisions.ForEach(d =>
                {
                    subDivisionItems.Add(new Models.SubDivisionNewItemReturn
                    {
                        Code = d.Code,
                        Id = d.Id,
                        Name = d.Name
                    });
                });

                return Ok(new SearchSubdivisionResultModel { SubDivisionsList = subDivisionItems, ItemsFound = subDivisions.Count });
            }
            catch (Exception ex)
            {
                return BadRequest(new ErrorModel { IsError = true, ErrorMessage = ex.Message + " " + ex.InnerException?.Message });
            }
        }

        /// <summary>
        /// Get a SubDivision by Id
        /// </summary>
        /// <param name="subDivisionId">SubDivision Id</param>
        /// <returns>SubDivision entity</returns>
        /// <response code="200">Returns the SubDivision entity</response>
        /// <response code="400">Get some Database error.</response>
        /// <response code="401">User doesn't have authorization to access.</response>
        /// <response code="404">If the SubDivision Id don't exist. An error entity is sent.</response>
        // GET: api/SubDivisions/5
        [HttpGet]
        [SwaggerOperation(Tags = new[] { tagSubDivision })]
        [Route("{countryId}/SubDivisions/{subDivisionId}")]
        public async Task<ActionResult<Models.SubDivisionNewItemReturn>> GetSubDivision(int subDivisionId)
        {
            try
            {
                var subDivision = await _context.SubDivisions.Include(d => d.Country).FirstOrDefaultAsync(d => d.Id == subDivisionId);

                if (subDivision == null)
                {
                    return NotFound(new ErrorModel { IsError = true, ErrorMessage = "SubDivision doesn't exist." });
                }

                SubDivisionNewItemReturn itemReturn = new SubDivisionNewItemReturn
                {
                    Code = subDivision.Code,
                    Id = subDivision.Id,
                    Name = subDivision.Name
                };
                return Ok(itemReturn);
            }
            catch (Exception ex)
            {

                return BadRequest(new ErrorModel { IsError = true, ErrorMessage = ex.Message + " " + ex.InnerException?.Message });
            }
        }

        /// <summary>
        /// Update SubDivision information
        /// </summary>
        /// <param name="SubDivisionId">SubDivision Id</param>
        /// <param name="subDivision">SubDivision entity to be Updated</param>
        /// <returns>SubDivision entity modified</returns>
        /// <response code="200">Returns the SubDivision entity updated</response>
        /// <response code="400">An error entity is sent.<br />If trying to update the name and is already assigned to other SubDivision entity.<br />If trying to update a SubDivision entity and doesn't exist.<br />If any error showed up during DB update operation.</response>
        /// <response code="401">User doesn't have authorization to access.</response>
        // PUT: api/SubDivisions/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut]
        [SwaggerOperation(Tags = new[] { tagSubDivision })]
        [Route("{countryId}/SubDivisions/{SubDivisionId}")]
        public async Task<IActionResult> PutSubDivision(int SubDivisionId, SubDivisionNewItem subDivision)
        {
            SubDivisionItemReturn entity = _context.SubDivisions.FirstOrDefault(d => d.Id == SubDivisionId);
            if (entity == null)
                return BadRequest(new ErrorModel { IsError = true, ErrorMessage = "SubDivision Id doesn't exist." });

            entity.Name = subDivision.Name;
            entity.Code = subDivision.Code;
            entity.Updated_At = DateTimeOffset.Now;

            try
            {
                await _context.SaveChangesAsync();
                return Ok(entity);
            }
            catch (Exception ex)
            {
                return BadRequest(new ErrorModel { IsError = true, ErrorMessage = ex.Message + " " + ex.InnerException?.Message });
            }
        }

        /// <summary>
        /// Create new SubDivision entity
        /// </summary>
        /// <param name="subDivision">SubDivision entity to create</param>
        /// <returns>New SubDivision entity</returns>
        /// <response code="201">Return new SubDivision entity.</response>
        /// <response code="400">If any error showed up during DB update operation</response>
        /// <response code="401">User doesn't have authorization to access.</response>
        /// <response code="404">SubDivision Id doesn't exist.</response>
        // POST: api/SubDivisions
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        [SwaggerOperation(Tags = new[] { tagSubDivision })]
        [Route("{countryId}/SubDivisions")]
        public async Task<ActionResult<Models.SubDivisionNewItemReturn>> PostSubDivision(SubDivisionNewItem subDivision)
        {
            try
            {
                int countryId = Convert.ToInt32(RouteData.Values["countryId"].ToString());

                if (SubDivisionExists(countryId, subDivision.Name, subDivision.Code))
                    return BadRequest(new ErrorModel { IsError = true, ErrorMessage = "The name or code code is already assigned to other SubDivision entity" });


                SubDivisionItemReturn division = new SubDivisionItemReturn
                {
                    Code = subDivision.Code,
                    CountryId = countryId,
                    Name = subDivision.Name,
                    Created_At = DateTimeOffset.Now,
                    Updated_At = DateTimeOffset.Now
                };


                _context.SubDivisions.Add(division);
                await _context.SaveChangesAsync();

                Models.SubDivisionNewItemReturn newItemReturn = new Models.SubDivisionNewItemReturn
                {
                    Id = division.Id,
                    Name = division.Name,
                    Code = division.Code
                };

                var routeValues = new { countryId, SubDivisions="SubDivisions", subDivisionId = division.Id };

                return CreatedAtAction("GetSubDivision", routeValues, newItemReturn);
            }
            catch (Exception ex)
            {
                return BadRequest(new ErrorModel { IsError = true, ErrorMessage = ex.Message + " " + ex.InnerException?.Message });
            }
        }

        /// <summary>
        /// Delete a SubDivision entity
        /// </summary>
        /// <param name="SubDivisionId">SubDivision id to delete</param>
        /// <returns>New SubDivision entity</returns>
        /// <response code="201">Return the deleted SubDivision entity.</response>
        /// <response code="400">If any error showed up during DB update operation</response>
        /// <response code="401">User doesn't have authorization to access.</response>
        /// <response code="404">SubDivision Id doesn't exist.</response>
        // DELETE: api/SubDivisions/5
        [HttpDelete]
        [SwaggerOperation(Tags = new[] { tagSubDivision })]
        [Route("{countryId}/SubDivisions/{SubDivisionId}")]
        public async Task<ActionResult<SubDivisionNewItemReturn>> DeleteSubDivision(int SubDivisionId)
        {
            try
            {
                var subDivision = await _context.SubDivisions.FindAsync(SubDivisionId);
                if (subDivision == null)
                {
                    return NotFound(new ErrorModel { IsError = true, ErrorMessage = "SubDivision Id doesn't exist." });
                }

                SubDivisionNewItemReturn itemReturn = new SubDivisionNewItemReturn
                {
                    Code = subDivision.Code,
                    Id = subDivision.Id,
                    Name = subDivision.Name
                };

                _context.SubDivisions.Remove(subDivision);
                await _context.SaveChangesAsync();

                return Ok(itemReturn);
            }
            catch (Exception ex)
            {

                return BadRequest(new ErrorModel { IsError = true, ErrorMessage = ex.Message + " " + ex.InnerException?.Message });
            }
        }

        #region Exists Validations
        //Check if the country name or alpha code haven't taken before
        private bool CountryExists(string name, string alpha2)
        {
            return _context.Countries.Any(d => d.Name.ToUpper() == name.ToUpper() || d.Alpha2.ToUpper() == alpha2.ToUpper());
        } 

        private bool SubDivisionExists(int countryId,string name, string code)
        {
            return _context.SubDivisions.Any(d => d.CountryId==countryId && (d.Name.ToUpper() == name.ToUpper() || d.Code.ToUpper() == code.ToUpper()));
        }
        #endregion
    }
}

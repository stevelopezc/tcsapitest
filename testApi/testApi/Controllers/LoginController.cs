﻿namespace testApi.Controllers
{
    using System;
    using System.IdentityModel.Tokens.Jwt;
    using System.Security.Claims;
    using System.Text;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Cors;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.IdentityModel.Tokens;
    using testApi.Data;
    using testApi.Entities;
    using testApi.Models;

    [EnableCors("TCSTestApiOrigins")]
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private readonly testApiContext _context;

        public LoginController(testApiContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Validate login information and get token authorization
        /// </summary>
        /// <param name="login">Login information</param>
        /// <returns>Token authorization</returns>
        /// <response code="200">Returns an authorization Token</response>
        /// <response code="400">Login information is missing</response>
        /// <response code="401">User credentials doesn't have authorization to access.</response>
        [HttpPost]
        [Route("Authenticate")]
        public async Task<ActionResult<TokenAuthorizationModel>> Authenticate([FromBody] LoginModel login)
        {
            if (login == null)
                return BadRequest(new ErrorModel { IsError = true, ErrorMessage = "There is some invalid values." });

            User user = await _context.Users.FirstOrDefaultAsync(d => d.UserName == login.UserName && d.Password == login.Password);
            if (user == null)
                return Unauthorized(new ErrorModel { IsError = true, ErrorMessage = "User or Password is Invalid." });

            var claims = new[]
                            {
                                new Claim(JwtRegisteredClaimNames.Sub, user.UserName),
                                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
                            };

            var expiration = DateTime.UtcNow.AddDays(15);

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("F215024856674892868C954DE8ACB8D3"));
            var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var token = new JwtSecurityToken(
                "tcsTestApi",
                "users",
                claims,
                expires: expiration,
                signingCredentials: credentials
            );

            return Ok(new TokenAuthorizationModel { Token = new JwtSecurityTokenHandler().WriteToken(token) , Expiration= expiration });
        }
    }
}

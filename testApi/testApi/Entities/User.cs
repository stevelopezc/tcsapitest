﻿namespace testApi.Entities
{
    using Newtonsoft.Json;
    using System;

    public class User
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("username")]
        public string UserName { get; set; }

        [JsonProperty("password")]
        public string Password { get; set; }

        [JsonProperty("created_at")]
        public DateTimeOffset Created_At { get; set; }

        [JsonProperty("updated_at")]
        public DateTimeOffset Updated_At { get; set; }
    }
}

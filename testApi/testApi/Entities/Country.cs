﻿namespace testApi.Entities
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using testApi.Models;

    public class Country : CountryNewItem
    {
        public Country()
        {
            SubDivisions = new HashSet<SubDivisionItemReturn>();
        }

        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("created_at")]
        public DateTimeOffset  Created_At { get; set; }
        
        [JsonProperty("updated_at")]
        public DateTimeOffset Updated_At { get; set; }

        public ICollection<SubDivisionItemReturn> SubDivisions{ get; set; }
    }
}

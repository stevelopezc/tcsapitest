﻿namespace testApi.Entities
{
    using Newtonsoft.Json;
    using System;
    using testApi.Models;

    public class SubDivisionItemReturn : SubDivisionNewItem
    {

        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("countryId")]
        public int CountryId { get; set; }

        public virtual Country Country { get; set; }

        [JsonProperty("created_at")]
        public DateTimeOffset Created_At { get; set; }

        [JsonProperty("updated_at")]
        public DateTimeOffset Updated_At { get; set; }
    }
}

﻿namespace testApi.Data
{
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Hosting;
    using Microsoft.EntityFrameworkCore;
    using System;
    using System.Linq;
    using testApi.Entities;

    public static class MigrationManager
    {
        public static IHost MigrateDatase(this IHost webHost)
        {
            using (var scope = webHost.Services.CreateScope())
            {
                using (var appContext = scope.ServiceProvider.GetRequiredService<testApiContext>())
                {
                    try
                    {
                        appContext.Database.Migrate();
                        LoadUser(appContext);
                        LoadTestCountries(appContext);
                        LoadTestSubDivisions(appContext);

                    }
                    catch (Exception ex)
                    {
                        //Log errors or do anything you think it's needed
                        Console.WriteLine(ex.Message + " " + ex.InnerException?.Message);
                        throw;
                    }
                }
            }
            return webHost;
        }

        private static void LoadUser(testApiContext appContext)
        {
            if (!appContext.Users.Any())
            {
                appContext.Users.Add(new User
                {
                    UserName = "loadTestUser",
                    Password = "@dminT3st",
                    Created_At = DateTimeOffset.Now,
                    Updated_At = DateTimeOffset.Now
                });
                appContext.SaveChanges();
            }
        }

        private static void LoadTestCountries(testApiContext appContext)
        {
            if (!appContext.Countries.Any())
            {
                appContext.Countries.Add(new Country
                {
                    Name = "Guatemala",
                    Alpha2 = "GT",
                    Alpha3 = "GTM",
                    ISO_3166_2 = "https://en.wikipedia.org/wiki/ISO_3166-2:GT",
                    Code = 320,
                    Is_Independent = true,
                    Created_At = DateTimeOffset.Now,
                    Updated_At = DateTimeOffset.Now
                });
                appContext.SaveChanges();
            }
        }

        private static void LoadTestSubDivisions(testApiContext appContext)
        {
            if (!appContext.SubDivisions.Any())
            {
                Country country = appContext.Countries.FirstOrDefault();
                appContext.SubDivisions.Add(new SubDivisionItemReturn
                {
                    Name = "Guatemala",
                    CountryId = country.Id,
                    Code = "GT",
                    Created_At = DateTimeOffset.Now,
                    Updated_At = DateTimeOffset.Now
                });
                appContext.SaveChanges();
            }
        }
    }
}

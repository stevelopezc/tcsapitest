﻿namespace testApi.Data
{
    using Microsoft.EntityFrameworkCore;
    using testApi.Entities;

    public class testApiContext : DbContext
    {
        public DbSet<Country> Countries { get; set; }
        public DbSet<SubDivisionItemReturn> SubDivisions { get; set; }
        public DbSet<User> Users { get; set; }

        public testApiContext(DbContextOptions<testApiContext> options)
            : base(options) 
        { }
    }
}

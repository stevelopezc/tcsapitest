
namespace XUnitTestTCSIntegration
{
    using Newtonsoft.Json;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Threading.Tasks;
    using testApi;
    using testApi.Entities;
    using testApi.Models;
    using Xunit;
    using XUnitTestTCS;

    public class CountryControllerTest : IClassFixture<TestAPIServer<Startup>>
    {
        private HttpClient Client;
        public TokenAuthorizationModel TokenAuthorization { get; set; }
        public CountryControllerTest(TestAPIServer<Startup> testAPIServer)
        {
            Client = testAPIServer.Client;
        }

        #region Country
        [Fact]
        public async Task TestGetCountries()
        {
            var request = "/api/Countries";
            Client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", await GetTokenAuth());
            var response = await Client.GetAsync(request);

            // Assert
            response.EnsureSuccessStatusCode();

        }

        [Fact]
        public async Task TestGetCountriesByFilter()
        {
            var request = "api/Countries/name=Gu&alpha2= ";
            Client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", await GetTokenAuth());
            var response = await Client.GetAsync(request);

            // Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task TestGetCountry()
        {
            var request = "/api/Countries/1";
            Client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", await GetTokenAuth());
            var response = await Client.GetAsync(request);

            // Assert
            response.EnsureSuccessStatusCode();

        }

        [Fact]
        public async Task TestDeleteCountry()
        {
            var request = "/api/Countries/7";
            Client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", await GetTokenAuth());
            var response = await Client.DeleteAsync(request);

            // Assert
            response.EnsureSuccessStatusCode();

        }

        [Fact]
        public async Task TestPostCountry()
        {
            var request = "/api/Countries";
            Client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", await GetTokenAuth());
            Country country = new Country
            {
                Alpha2 = "GE",
                Alpha3 = "GER",
                Code = 380,
                Is_Independent = true,
                ISO_3166_2 = "https://en.wikipedia.org/wiki/ISO_3166-2:GE",
                Name = "Germany"
            };
            var response = await Client.PostAsync(request, ContentHelper.GetStringContent(country));

            // Assert
            response.EnsureSuccessStatusCode();

        }

        [Fact]
        public async Task TestPutCountry()
        {
            var request = "/api/Countries/1";
            Client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", await GetTokenAuth());
            Country country = new Country
            {
                Id = 1,
                Alpha2 = "GT",
                Alpha3 = "GTM",
                Code = 380,
                Is_Independent = true,
                ISO_3166_2 = "https://en.wikipedia.org/wiki/ISO_3166-2:GT",
                Name = "Guatemala"
            };
            var response = await Client.PutAsync(request, ContentHelper.GetStringContent(country));

            // Assert
            response.EnsureSuccessStatusCode();

        }
        #endregion

        #region Country/SubDivisions
        [Fact]
        public async Task TestGetSubDivisionByCountry()
        {
            var request = "/api/countries/1/SubDivisions";
            Client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", await GetTokenAuth());
            var response = await Client.GetAsync(request);

            // Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task TestGetSubDivision()
        {
            var request = "/api/countries/1/SubDivisions/1";
            Client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", await GetTokenAuth());
            var response = await Client.GetAsync(request);

            // Assert
            response.EnsureSuccessStatusCode();

        }

        [Fact]
        public async Task Test3DeleteSubDivision()
        {
            var request = "/api/countries/1/SubDivisions/25";
            Client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", await GetTokenAuth());
            var response = await Client.DeleteAsync(request);

            // Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task Test1PostSubDivisions()
        {
            var request = "/api/countries/1/SubDivisions";
            Client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", await GetTokenAuth());
            testApi.Entities.SubDivisionItemReturn subDivision = new testApi.Entities.SubDivisionItemReturn
            {
                Code = "SBP",
                CountryId = 1,
                Name = "San Benito"
            };
            var response = await Client.PostAsync(request, ContentHelper.GetStringContent(subDivision));

            // Assert
            response.EnsureSuccessStatusCode();

        }

        [Fact]
        public async Task Test2PutSubDivision()
        {
            var request = "/api/countries/1/SubDivisions/25";
            Client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", await GetTokenAuth());
            SubDivisionNewItem country = new SubDivisionNewItem
            {
                Name = "San Benito Peten",
                Code="SBP"
            };
            var response = await Client.PutAsync(request, ContentHelper.GetStringContent(country));

            // Assert
            response.EnsureSuccessStatusCode();

        }

        
        #endregion
        private async Task<string> GetTokenAuth()
        {
            LoginModel login = new LoginModel { UserName = "loadTestUser", Password = "@dminT3st" };
            var request = "/api/Login/Authenticate";

            var response = await Client.PostAsync(request, ContentHelper.GetStringContent(login));
            var value = await response.Content.ReadAsStringAsync();
            TokenAuthorization = JsonConvert.DeserializeObject<TokenAuthorizationModel>(value);
            return TokenAuthorization.Token;
        }
    }
}

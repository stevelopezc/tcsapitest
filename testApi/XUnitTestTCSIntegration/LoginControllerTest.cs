namespace XUnitTestTCSIntegration
{
    using System.Net.Http;
    using System.Threading.Tasks;
    using testApi;
    using testApi.Models;
    using Xunit;
    using XUnitTestTCS;

    public class LoginControllerTest : IClassFixture<TestAPIServer<Startup>>
    {
        private HttpClient Client;
        public LoginControllerTest(TestAPIServer<Startup> testAPIServer)
        {
            Client = testAPIServer.Client;
        }

        [Fact]
        public async Task TestAuthorization()
        {
            LoginModel login = new LoginModel { UserName = "loadTestUser", Password = "@dminT3st" };
            var request = "/api/Login/Authenticate";
            
            var response = await Client.PostAsync(request, ContentHelper.GetStringContent(login));
            var value = await response.Content.ReadAsStringAsync();
            response.EnsureSuccessStatusCode();
        }

    }
}
